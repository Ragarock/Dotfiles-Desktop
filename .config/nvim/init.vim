call plug#begin('~/.local/share/nvim/plugged')

Plug 'majutsushi/tagbar'
Plug 'jsfaint/gen_tags.vim'
Plug 'arakashic/chromatica.nvim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'beyondmarc/glsl.vim'

call plug#end()

colorscheme elflord

let g:chromatica#libclang_path='/usr/lib/libclang.so.3.9'
let g:chromatica#enable_at_startup=1
let g:chromatica#responsive_mode=1

syntax enable
set ruler
set hidden
set number
set nowrap

" Bindings
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>
nmap <silent> <C-r> :e %<CR>

nmap <F4> :make <CR>
nmap <F5> :make run <CR>

nmap <leader>l :TagbarToggle<CR>

nnoremap tn :tabnew<Space>
nnoremap tk :tabnext<CR>
nnoremap tj :tabprev<CR>
nnoremap th :tabfirst<CR>
nnoremap tl :tablast<CR>

set autoindent
set noexpandtab
set tabstop=2
set shiftwidth=2
set softtabstop=0

let g:ctrlp_map = '<leader>t'
let g:ctrlp_use_caching=1

" File Specific settings
augroup rust_files
	autocmd!
	autocmd FileType rust setlocal autoindent
	autocmd FileType rust setlocal noexpandtab
	autocmd FileType rust set      tabstop=2
	autocmd FileType rust set      shiftwidth=2
	autocmd FileType rust set      softtabstop=0
augroup end

augroup python_files
	autocmd!
	autocmd FileType python setlocal autoindent
	autocmd FileType python setlocal noexpandtab
	autocmd FileType python set      tabstop=2
	autocmd FileType python set      shiftwidth=2
	autocmd FileType python set      softtabstop=0
augroup end

let g:glsl_default_version = 'glsl330'
let g:glsl_file_extensions = '*.glsl'
